using System;
using System.IO;
using System.Reflection;
using System.Resources;

namespace ModularCheckers.util
{
    /// <summary>
    /// Allows to easily pick up serialized Classes.
    /// </summary>
    public class ConfPathHelper
    {
        private const string FileSeparator = "/";

        private const string BaseDir = "gameData";
        private static string CheckersSerialized = "checkersBoard";
        private const string ColorsSerialized = "colors";
        private const string FileExtension = ".ser";

        private ConfPathHelper() { }

        /// <summary>
        /// Returns BinaryReader for the Serialized Colors resource.
        /// </summary>
        /// <returns>BinaryReader of the resource</returns>
        public static FileStream getColors()
        {
            throw new NotImplementedException();
            /*var resource =
                new ResourceReader(FileSeparator + BaseDir + FileSeparator + ColorsSerialized + FileExtension);
            resource.
            return new FileStream(, FileMode.Open);*/
        }
    }
}