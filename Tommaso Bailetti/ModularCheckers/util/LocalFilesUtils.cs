using System;
using System.IO;

namespace ModularCheckers.util
{
    /// <summary>
    /// Assures the PC has every folder it may need.
    /// </summary>
public class LocalFilesUtils
    {
        /// <summary>
        /// Base dir in $HOME folder.
        /// </summary>
        public static string BaseDir = ".modularCheckers";

        /// <summary>
        /// History Directory.
        /// </summary>
        public static string HistoryDir = "history";

        private LocalFilesUtils()
        {
        }

        /// <summary>
        /// Checks for the home directory and tries to create it if it's missing.
        /// </summary>
        public static void CheckDirStructure()
        {
            if (!Directory.Exists(Environment.SpecialFolder.UserProfile + Path.PathSeparator + BaseDir))
            {
                Directory.CreateDirectory(Environment.SpecialFolder.UserProfile + Path.PathSeparator + BaseDir);
                if (!Directory.Exists(Environment.SpecialFolder.UserProfile + Path.PathSeparator + BaseDir +
                                      Path.PathSeparator + HistoryDir))
                {
                    Directory.CreateDirectory(Environment.SpecialFolder.UserProfile + Path.PathSeparator + BaseDir +
                                              Path.PathSeparator + HistoryDir);
                }
            }
        }

        /// <summary>
        /// Returns the full path of the history folder.
        /// </summary>
        /// <returns>String of the path.</returns>
        public static string GetHistoryPath()
        {
            return Environment.SpecialFolder.UserProfile + Path.PathSeparator + BaseDir + Path.PathSeparator +
                   HistoryDir;
        }
    }
}