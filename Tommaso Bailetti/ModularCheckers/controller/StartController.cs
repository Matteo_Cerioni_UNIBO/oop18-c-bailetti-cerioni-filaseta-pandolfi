using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ModularCheckers.model;
using ModularCheckers.view;
using ModularCheckers.view.observers;

namespace ModularCheckers.controller
{
    public class StartController : IStartViewObservable
    {
        private List<IPlayer> _players;
        private GameType? _gameType;
        
        public StartController()
        {
            new StartView(this);
        }

        public List<IPlayer> InsertPlayers(Dictionary<string, PlayerType> players)
        {
            // TODO ask Viroli about the cast, why is needed?
            _players = players.Select(e =>
                e.Value.Equals(PlayerType.HumanPlayer)
                    ? (IPlayer) new HumanPlayer(e.Key)
                    : (IPlayer) new RandomPlayer(e.Key)).ToList();
            return _players;
        }

        public void ChooseGame(GameType game)
        {
            _gameType = game;
        }

        public void Exit()
        { 
            Environment.Exit(0);
        }

        public void NewGame()
        {
            if (_gameType.HasValue && _players.Count != 0)
            {
                try
                {
                    new GameLoopImpl(_players, _gameType.Value).StartLoop();
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}