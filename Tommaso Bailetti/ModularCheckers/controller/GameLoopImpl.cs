using System;
using System.Collections.Generic;
using ModularCheckers.model;
using ModularCheckers.view;
using ModularCheckers.view.observers;

namespace ModularCheckers.controller
{
    public class GameLoopImpl : IGameLoop, IGameTableViewObservable
    {

        public GameLoopImpl(List<IPlayer> players, GameType gameType)
        {
            new GameTableView(this);
        }

        public void StartLoop()
        {
            throw new NotImplementedException();
        }

        public void Surrender()
        {
            throw new NotImplementedException();
        }

        public void SelectPiece(ICoordinate coordinate)
        {
            throw new NotImplementedException();
        }

        public void MakeStepChosen(ICoordinate whereToMove)
        {
            throw new NotImplementedException();
        }
    }
}