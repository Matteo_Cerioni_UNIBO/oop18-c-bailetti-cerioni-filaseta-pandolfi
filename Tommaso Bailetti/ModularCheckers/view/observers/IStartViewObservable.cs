using System;
using System.Collections.Generic;
using ModularCheckers.model;

namespace ModularCheckers.view.observers
{
    public interface IStartViewObservable
    {
        List<IPlayer> InsertPlayers(Dictionary<string, PlayerType> players);

        void ChooseGame(GameType game);

        void Exit();

        void NewGame();
    }
}