using ModularCheckers.model;

namespace ModularCheckers.view.observers
{
    public interface IGameTableViewObservable
    {
        void Surrender();

        void SelectPiece(ICoordinate? coordinate);

        void MakeStepChosen(ICoordinate whereToMove);
    }
} 