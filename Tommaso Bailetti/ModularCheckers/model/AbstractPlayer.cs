namespace ModularCheckers.model
{
    public abstract class AbstractPlayer : IPlayer
    {
        private string _name;

        protected AbstractPlayer(string name)
        {
            _name = name;
        }
    }
}