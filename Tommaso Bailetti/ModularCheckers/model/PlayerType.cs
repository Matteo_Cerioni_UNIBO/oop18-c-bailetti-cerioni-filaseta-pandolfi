namespace ModularCheckers.model
{
    public enum PlayerType
    {
        HumanPlayer,
        RandomPlayer,
        HistoryPlayer
    }
}