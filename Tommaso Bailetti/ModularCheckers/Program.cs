﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModularCheckers.controller;
using ModularCheckers.model;

namespace ModularCheckers
{
    class Program
    {
        static void Main(string[] args)
        {
            new StartController();
        }
    }
}