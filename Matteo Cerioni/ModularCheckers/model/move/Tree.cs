using System;
using System.Collections.Generic;
using System.Linq;

namespace ModularCheckers.model.move
{
    public class Tree<TX> : ITree<TX>
    {
        private readonly TX _root;
        private readonly IList<ITree<TX>> _childrens;
        
        /// <summary>
        /// Create a Tree and its children.
        /// </summary>
        /// <param name="root">the value of the root.</param>
        /// <param name="childrens">all the sub-trees.</param>
        public Tree(TX root, IList<ITree<TX>> childrens)
        {
            this._root = root;
            this._childrens = childrens;
        }
        
        /// <summary>
        /// Create an empty Tree.
        /// </summary>
        /// <param name="root">the value of the root.</param>
        public Tree(TX root)
        {
            this._root = root;
            this._childrens = new List<ITree<TX>>();
        }
        
        /// <inheritdoc/>
        public TX GetRoot()
        {
            return this._root;
        }

        /// <inheritdoc/>
        public IList<ITree<TX>> GetChildren()
        {
            return this._childrens;
        }
        
        /// <inheritdoc/>
        public IList<ITree<TX>> GetAllNodes()
        {
            IList<ITree<TX>> nodes = new List<ITree<TX>>();
            nodes.Add(this);
            foreach (ITree<TX> children in _childrens)
            {
                IList<ITree<TX>> childrenNodes = children.GetAllNodes();
                foreach (ITree<TX> childrenNode in childrenNodes)
                {
                    nodes.Add(childrenNode);
                }
            }

            return nodes;
        }

        /// <inheritdoc/>
        public IList<TX> GetAllValues()
        {
            return this.GetAllNodes().Select(tree => tree.GetRoot()).ToList();
        }
        
        /// <inheritdoc/>
        public IList<TX> GetFirstChildren()
        {
            return this.GetChildren().Select(tree => tree.GetRoot()).ToList();
        }

        /// <inheritdoc/>
        public int Height()
        {
            if (this.GetChildren().Count > 0)
            {
                return this.GetChildren().Select(tree => tree.Height() + 1).Max();
            } else {
                return 1;
            }
        }

        /// <inheritdoc/>
        public bool BalanceToHeight(int maxHeight)
        {
            for (int i = 0; i < this.GetChildren().Count; i++) 
            {
                if (!this.GetChildren().ElementAt(i).BalanceToHeight(maxHeight - 1)) 
                {
                    this.GetChildren().RemoveAt(i);
                    i--;
                }
            }
            if (this.GetChildren().Count > 0) 
            {
                return true;
            }
            return maxHeight <= 1;
        }

        /// <inheritdoc/>
        public int? LevelOfFirstAppearance(Func<ITree<TX>,Boolean> condition)
        {
            IList<ITree<TX>> list = new List<ITree<TX>>();
            list.Add(this);
            int level = 1;
            while (list.Count > 0) 
            {
                if(list.Where(condition).Count() > 0)
                {
                    return level;
                } else
                {
                    list = list.SelectMany(tree => tree.GetChildren()).ToList();
                    level++;
                }
            }
            return null;
        }

        /// <inheritdoc/>
        public int NumberOfNodesForCondition(Func<ITree<TX>,Boolean> condition)
        {
            return (condition.Invoke(this) ? 1 : 0)
                   + this.GetChildren().Select(tree => tree.NumberOfNodesForCondition(condition)).Sum();
        }
    }
}