using System;
using ModularCheckers.model.piece;

namespace ModularCheckers.model.move
{
    /// <summary>
    /// Basic implementation of Step.    
    /// </summary>
    [Serializable]
    public class Step:IStep
    {
        private readonly Coordinate _coordinate;
        private Pair<Coordinate, IPiece>? _deadPiece = null;

        /// <param name="coordinate">The coordinate of the piece in this step.</param>
        public Step(Coordinate coordinate)
        {
            _coordinate = coordinate;
        }

        /// <param name="coordinate">The coordinate of the piece in this step.</param>
        /// <param name="deadPiece">The information on the dead piece in this step.</param>
        public Step(Coordinate coordinate, Pair<Coordinate, IPiece> deadPiece)
        {
            _coordinate = coordinate;
            _deadPiece = deadPiece;
        }

        /// <inheritdoc/>
        public Coordinate GetCoordinate()
        {
            return _coordinate;
        }
        
        /// <inheritdoc/>
        public Pair<Coordinate, IPiece>? GetDeadPiece()
        {
            return _deadPiece;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return "Step: [Coordinate: (" + GetCoordinate() + "), Dead Piece: ("
                   + (GetDeadPiece().HasValue?
                       GetDeadPiece().Value.X + " " + GetDeadPiece().Value.Y
                       : "None")
                   + ")]";
        }
        
        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + _coordinate.GetHashCode();
            result = prime * result + (_deadPiece.HasValue ? _deadPiece.Value.GetHashCode() : 0);
            return result;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj is Step)) {
                return false;
            }
            Step other = (Step) obj;
            if (!_coordinate.Equals(other.GetCoordinate())) {
                return false;
            }
            if (!_deadPiece.HasValue) {
                if (other.GetDeadPiece() != null) {
                    return false;
                }
            } else if (!_deadPiece.Equals(other.GetDeadPiece())) {
                return false;
            }
            return true;
        }
    }
}