using System;
using System.Collections.Generic;

namespace ModularCheckers.model.move
{
    /// <summary>
    /// Represents a generic tree.
    /// </summary>
    /// <typeparam name="TX">the generic class of the nodes.</typeparam>
    public interface ITree<TX>
    {
        /// <summary>
        /// Get the root of the tree. 
        /// </summary>
        /// <returns>the root of the tree.</returns>
        TX GetRoot();

        /// <summary>
        /// Get all the children of the tree. 
        /// </summary>
        /// <returns>a list containing the children of the tree.</returns>
        IList<ITree<TX>> GetChildren();

        /// <summary>
        /// get of value of the tree in a list.
        /// </summary>
        /// <returns>a List containing all the nodes.</returns>
        IList<TX> GetAllValues();
        
        /// <summary>
        /// Get all nodes of the tree in a list.
        /// </summary>
        /// <returns>a List containing all the nodes as Tree.</returns>
        IList<ITree<TX>> GetAllNodes();

        /// <summary>
        /// Return all the children of the first level of the tree.
        /// </summary>
        /// <returns>a list containing only the first level of the tree.</returns>
        IList<TX> GetFirstChildren();

        /// <summary>
        /// Get the height of the tree
        /// </summary>
        /// <returns>the height of the Tree.</returns>
        int Height();

        /// <summary>
        /// Trim a tree, by leaving only branches of at least height maxHeight.
        /// </summary>
        /// <param name="maxHeight">height of the final tree.</param>
        /// <returns>true if remains at least one branch of height maxHeight or greater.</returns>
        bool BalanceToHeight(int maxHeight);

        /// <summary>
        /// Find the level of the Tree where a node respect a certain condition.
        /// </summary>
        /// <param name="condition">the condition to check on a Node.</param>
        /// <returns>the minimum level the Tree when the condition passed is found.</returns>
        int? LevelOfFirstAppearance(Func<ITree<TX>,Boolean> condition);

        /// <summary>
        /// Return the number of nodes that respects the condition in the Tree. Root is included.
        /// </summary>
        /// <param name="condition">the condition to check.</param>
        /// <returns>the number of nodes that respects the condition.</returns>
        int NumberOfNodesForCondition(Func<ITree<TX>,Boolean> condition);
    }
}