using System.Collections.Generic;
using System.Collections.Immutable;

namespace ModularCheckers.model.move
{
    /// <summary>
    /// Basic move implementation
    /// </summary>
    public class Move : IMove
    {
        private readonly IList<IStep> _stepList;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="stepList">stepList the list containing all the step to generate the move.</param>
        public Move(IList<IStep> stepList)
        {
            this._stepList = stepList;
        }
        
        /// <inheritdoc />
        public IList<IStep> GetSteps()
        {
            return _stepList.ToImmutableList();
        }
    }
}