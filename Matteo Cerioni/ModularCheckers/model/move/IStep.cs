using ModularCheckers.model.piece;

namespace ModularCheckers.model.move
{
    public interface IStep
    {
        /// <summary>
        /// Get the coordinate of the piece in the step. Its position if the step is the first,
        /// where to move in the next steps.
        /// </summary>
        /// <returns>the coordinate of the piece in the step.</returns>
        Coordinate GetCoordinate();
        
        /// <summary>
        /// Get the coordinate and the piece killed in the step. 
        /// </summary>
        /// <returns>the dead piece of the move.</returns>
        Pair<Coordinate, IPiece>? GetDeadPiece();
    }
}