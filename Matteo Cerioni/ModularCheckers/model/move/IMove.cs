using System.Collections.Generic;

namespace ModularCheckers.model.move
{
    /// <summary>
    /// Interface for the Move Class. The first move contains the starting coordinate
    /// of the piece moving. The following contains all the coordinate where the
    /// piece is going.
    /// </summary>
    public interface IMove
    {
        /// <summary>
        /// Get a list containing all the steps done in a move.
        /// </summary>
        /// <returns>an unmodifiable list containing all the steps of the move.</returns>
        IList<IStep> GetSteps();
    }
}