using System;

namespace ModularCheckers.model
{
    /// <summary>
    /// Coordinate class is a Pair of integers.
    /// </summary>
    [Serializable]
    public struct Coordinate: IPair<int, int>
    {
        /// <summary>
        /// first element of the coordinates
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// second element of the coordinates
        /// </summary>
        public int Y { get; set; }
        
        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="x">First element</param>
        /// <param name="y">Second element</param>
        public Coordinate(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        
        /// <summary> Invert the Y element.</summary>
        public void InvertY() {
            Y *= -1;
        }
        
        /// <summary> Invert the Y element.</summary>
        public void InvertX() {
            X *= -1;
        }

        /// <summary>
        /// Translate this coordinate to another.
        /// </summary>
        /// <param name="to">coordinate to translate</param>
        public void Translate(Coordinate to)
        {
            this.Y += to.Y;
            this.X += to.X;
        }
        
        /// <summary>
        /// Check if the coordinate is inside of the specified borders.
        /// </summary>
        /// <param name="minX">minimum x value</param>
        /// <param name="minY">minimum y value</param>
        /// <param name="maxX">maximum x value</param>
        /// <param name="maxY">maximum y value</param>
        /// <returns>true if is inside, else false.</returns>
        public bool IsInsideBorders(int minX, int minY, int maxX, int maxY) {
            return this.X >= minX && this.X <= maxX && this.Y >= minY && this.Y <= maxY;
        }
        
        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + ((X == null) ? 0 : X.GetHashCode());
            result = prime * result + ((Y == null) ? 0 : Y.GetHashCode());
            return result;
        }
    }
}