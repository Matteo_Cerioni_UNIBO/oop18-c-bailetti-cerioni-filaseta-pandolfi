using System;

namespace ModularCheckers.model
{
    /// <summary> Implementation of Pair</summary>
    /// <typeparam name="TX">Type of the first element</typeparam>
    /// <typeparam name="TY">Type of the second element</typeparam>
    [Serializable]
    public struct Pair<TX, TY>: IPair<TX, TY>
    {
        /// <inheritdoc />
        public TX X { get; set; }
        /// <inheritdoc />
        public TY Y { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="x">First element</param>
        /// <param name="y">Second element</param>
        public Pair(TX x, TY y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + ((X == null) ? 0 : X.GetHashCode());
            result = prime * result + ((Y == null) ? 0 : Y.GetHashCode());
            return result;
        }
    }
}