using System.Collections.Generic;

namespace ModularCheckers.model.piece
{
    /// <summary>
    /// Abstract implementation of piece implementing GetColor, equals & hashCode.
    /// </summary>
    public abstract class AbstractPiece : IPiece
    {
        private readonly Color color;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="color">The piece color</param>
        protected AbstractPiece(Color color)
        {
            this.color = color;
        }

        /// <inheritdoc />
        public Color GetColor()
        {
            return this.color;
        }

        /// <inheritdoc />
        public abstract PieceType GetPieceType();

        /// <inheritdoc />
        public abstract IEnumerable<Coordinate> GetMoveSet();

        /// <inheritdoc />
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + (color == null ? 0 : color.GetHashCode());
            return result;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (GetType() != obj.GetType()) {
                return false;
            }
            AbstractPiece other = (AbstractPiece) obj;

            return color == other.color;
        }
    }
}