namespace ModularCheckers.model.piece
{
    /// <summary>
    /// Enum of type of pieces.
    /// </summary>
    public enum PieceType
    {
        /// <summary>
        /// Man type
        /// </summary>
        Man,
        /// <summary>
        /// King type
        /// </summary>
        KingG
    }
}