using System.Collections.Generic;

namespace ModularCheckers.model.piece
{
    /// <summary>
    /// The Man Piece class
    /// </summary>
    public class Man : AbstractPiece
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="color"> The color of the man.</param>
        public Man(Color color) : base(color)
        {
        }

        /// <inheritdoc />
        public override PieceType GetPieceType()
        {
            return PieceType.Man;
        }

        /// <inheritdoc />
        public override IEnumerable<Coordinate> GetMoveSet()
        {
            return new List<Coordinate>(){new Coordinate(1,1), new Coordinate(-1,1)};
        }
    }
}