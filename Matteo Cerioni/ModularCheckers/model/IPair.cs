using System;

namespace ModularCheckers.model
{
    /// <summary>Interface of a Pair</summary>
    /// <typeparam name="TX">Type of the first element</typeparam>
    /// <typeparam name="TY">Type of the second element</typeparam>
    public interface IPair<TX, TY>
    {
        /// <summary>The first element of the pair of type TX</summary>
        TX X { get; set; }
        /// <summary>The second element of the pair of type TY</summary>
        TY Y { get; set; }
    }
}