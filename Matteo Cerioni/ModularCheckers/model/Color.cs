namespace ModularCheckers.model
{
    /// <summary>
    /// Enum of colors.
    /// </summary>
    public enum Color
    {
        /// <summary>
        /// White color.
        /// </summary>
        White,
        /// <summary>
        /// Black color.
        /// </summary>
        Black
    }
}