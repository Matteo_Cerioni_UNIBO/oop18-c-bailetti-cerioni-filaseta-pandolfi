using ModularCheckers.model;
using ModularCheckers.model.move;
using ModularCheckers.model.piece;
using NUnit.Framework;

namespace ModularCheckers.test
{
    /// <summary>Test the Step class</summary>
    [TestFixture]
    public class TestStep
    {
        /// <summary>
        /// Testing step constructors, getters and setters.
        /// </summary>
        [Test]
        public void TestBasic()
        {
            IStep step = new Step(new Coordinate(1,1));
            Assert.AreEqual(step.GetCoordinate(), new Coordinate(1,1), "Coordinates must be (1,1)");
            Assert.IsNull(step.GetDeadPiece(), "Dead piece must be null.");
        }
        
        /// <summary>
        /// Testing dead piece.
        /// </summary>
        [Test]
        public void TestDeadPiece()
        {
            IPiece deadPiece = new Man(Color.White);
            Coordinate deadCoordinate = new Coordinate(4,5);
            IStep step = new Step(new Coordinate(5,5), new Pair<Coordinate, IPiece>(deadCoordinate, deadPiece));
            Assert.AreEqual(step.GetDeadPiece().Value.X, deadCoordinate, "Wrong dead coordinate.");
            Assert.AreEqual(step.GetDeadPiece().Value.Y, deadPiece, "Wrong dead piece.");
        }
    }
}