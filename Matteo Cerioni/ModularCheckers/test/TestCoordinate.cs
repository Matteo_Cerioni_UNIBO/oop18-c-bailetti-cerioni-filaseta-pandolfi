﻿using ModularCheckers.model;
using NUnit.Framework;

namespace ModularCheckers.test
{
    /// <summary>Test the Coordinate class</summary>
    [TestFixture]
    public class TestCoordinate
    {
        /// <summary>
        /// Testing coordinate constructor & properties.
        /// </summary>
        [Test]
        public void TestBasic()
        {
            Coordinate c = new Coordinate(5, 10);
            Assert.AreEqual(5, c.X, "X must be equals to 5.");
            Assert.AreEqual(10, c.Y, "Y must be equals to 10.");
            c.X = 15;
            Assert.AreEqual(15, c.X, "X now must be equals to 15.");
        }
        
        /// <summary>
        /// Testing coordinate copy of value.
        /// </summary>
        [Test]
        public void TestStruct()
        {
            Coordinate c1 = new Coordinate(5, 10);
            Coordinate c2 = c1;
            Assert.AreEqual(c1, c2, "The 2 coordinates must be equals.");
            c1.X = 15;
            Assert.AreNotEqual(c1, c2, "The 2 coordinates must be different.");
        }

        /// <summary>
        /// Testing inverts, translate and isInsideBorder methods.
        /// </summary>
        [Test]
        public void TestMethods()
        {
            Coordinate c1 = new Coordinate(5, 10);
            c1.InvertX();
            Assert.AreEqual(-5, c1.X, "X must be equals to -5 after invertX().");
            c1.InvertY();
            Assert.AreEqual(-10, c1.Y, "Y must be equals to -10 after invertY().");
            c1.Translate(new Coordinate(100,50));
            Assert.AreEqual(95, c1.X, "X must be equals to 95 after translation.");
            Assert.AreEqual(40, c1.Y, "Y must be equals to 40 after translation.");
            Assert.IsTrue(c1.IsInsideBorders(95,30,100,100), "Coordinate is inside specified borders");
            Assert.IsFalse(c1.IsInsideBorders(100,30,105,100), "Coordinate is outside specified borders");
        }

        [Test]
        public void TestEquals()
        {
            Coordinate c1 = new Coordinate(1, 1);
            Coordinate c2 = new Coordinate(1, 1);
            Assert.AreEqual(c1,c2, "Values must be equals.");
        }
    }
}