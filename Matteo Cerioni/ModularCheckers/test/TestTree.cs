using System.Collections.Generic;
using ModularCheckers.model.move;
using NUnit.Framework;

namespace ModularCheckers.test
{
    /// <summary>Test the Tree class</summary>
    [TestFixture]
    public class TestTree
    {
        /// <summary>
        /// Test the one level tree functionality.
        /// </summary>
        [Test]
        public void TestOneLevelTree() {
            ITree<int> t1 = new Tree<int>(5);
            Assert.AreEqual(5, t1.GetRoot(),"Root value is changed");
            Assert.AreEqual(0, t1.GetChildren().Count,"Childrens default must be empty");
            Assert.AreEqual(0, t1.GetFirstChildren().Count,"First Childrens must be empty");
            Assert.AreEqual(1, t1.Height(),"Height of one level tree must be 1");

            // testing all nodes list
            IList<int> allNodes = t1.GetAllValues();
            Assert.AreEqual(1, allNodes.Count, "nodes number must be 1");
            Assert.IsTrue(allNodes.Contains(5),"nodes not contain the right element");
        }
        
        /// <summary>
        /// Testing multiple levels tree functionality. 
        /// </summary>
        [Test]
        public void TestMultipleLevelsTree() {
            ITree<string> t = initMultipleLevelTree();
            Assert.AreEqual("A", t.GetRoot(), "Root value is changed");
            Assert.AreEqual(3, t.GetFirstChildren().Count, "First children of A must be 3");
            Assert.True(t.GetFirstChildren().Contains("AA"), "First children of A must contain AA");
            IList<ITree<string>> childrens = t.GetChildren();
            Assert.AreEqual(3, childrens.Count, "The children of A must be 3");
            ITree<string> aa = childrens[0];
            Assert.AreEqual("AA", aa.GetRoot(), "The first children of A must be AA");
            Assert.AreEqual(0 ,aa.GetChildren().Count, "AA should not contain children");

            ITree<string> ab = childrens[1];
            Assert.AreEqual("AB", ab.GetRoot(), "The second children of A must be AB");
            Assert.AreEqual(3, ab.GetChildren().Count, "AB should contain 3 children");

            ITree<string> ac = childrens[2];
            Assert.AreEqual("AC", ac.GetRoot(),"The third children of A must be AC");
            Assert.AreEqual(1, ac.GetChildren().Count, "AC should contain 1 children");

            // testing height
            Assert.AreEqual(3, t.Height(), "Tree height must be 3");
        }
        
        /// <summary>
        /// Initialize a tree like this. A (root) -AA -AB -ABA -ABB -ABC -AC -ACA
        /// </summary>
        /// <returns>the multilevel tree</returns>
        private ITree<string> initMultipleLevelTree() {
            IList<ITree<string>> childrens;

            ITree<string> aa = new Tree<string>("AA");

            ITree<string> aba = new Tree<string>("ABA");
            ITree<string> abb = new Tree<string>("ABB");
            ITree<string> abc = new Tree<string>("ABC");
            childrens = new List<ITree<string>>();
            childrens.Add(aba);
            childrens.Add(abb);
            childrens.Add(abc);
            ITree<string> ab = new Tree<string>("AB", childrens);

            ITree<string> aca = new Tree<string>("ACA");
            childrens = new List<ITree<string>>();
            childrens.Add(aca);
            ITree<string> ac = new Tree<string>("AC", childrens);

            childrens = new List<ITree<string>>();
            childrens.Add(aa);
            childrens.Add(ab);
            childrens.Add(ac);
            return new Tree<string>("A", childrens);
        }
    }
}