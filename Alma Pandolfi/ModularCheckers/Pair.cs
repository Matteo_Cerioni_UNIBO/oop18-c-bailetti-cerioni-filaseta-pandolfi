﻿using System;

namespace ModularCheckers
{
    [Serializable]
    /// <summary>
    /// Pair class.
    /// </summary>
    /// <typeparam name="TX"></typeparam>
    /// <typeparam name="TY"></typeparam>
    public class Pair<TX, TY>
    {
        private TX x;
        private TY y;

        /// <summary>
        /// Pair default constructor.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Pair(TX x,TY y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Get pair x element.
        /// </summary>
        /// <returns> x element </returns>
        public TX GetX()
        {
            return x;
        }

        /// <summary>
        /// Get pair y element.
        /// </summary>
        /// <returns> y element </returns>
        public TY GetY()
        {
            return y;
        }

        /// <summary>
        /// Set the x element.
        /// </summary>
        /// <param name="x"> the x to set </param>
        public void SetX(TX x)
        {
            this.x = x;

        }

        /// <summary>
        /// Set the y element.
        /// </summary>
        /// <param name="y"> the y to set </param>
        public void SetY(TY y)
        {
            this.y = y;

        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            result = prime * result + ((x == null) ? 0 : x.GetHashCode());
            result = prime * result + ((y == null) ? 0 : y.GetHashCode());
            return result;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (!(obj is Pair<TX,TY>)) {
                return false;
            }

            Pair<TX, TY> other = (Pair <TX, TY>) obj;

            if (x == null)
            {
                if (other.x != null)
                {
                    return false;
                }
            }
            else if (!x.Equals(other.x))
            {
                return false;
            }
            if (y == null)
            {
                if (other.y != null)
                {
                    return false;
                }
            }
            else if (!y.Equals(other.y))
            {
                return false;
            }
            return true;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return "[" + x + ";" + y + "]";
        }
    }
}