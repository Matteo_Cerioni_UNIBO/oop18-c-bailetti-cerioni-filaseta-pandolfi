﻿namespace ModularCheckers
{
    /// <summary>
    /// Type Enum.
    /// Types that can be used.
    /// </summary>
    enum PieceType
    {
        /// <summary>
        /// Man type.
        /// </summary>
        Man,

        /// <summary>
        /// King type.
        /// </summary>
        King
    }
}
