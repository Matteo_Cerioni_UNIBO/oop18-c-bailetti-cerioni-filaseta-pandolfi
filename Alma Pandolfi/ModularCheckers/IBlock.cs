﻿namespace ModularCheckers
{
    /// <summary>
    /// The block of a chessboard.
    /// </summary>
    /// 
    interface IBlock
    {
        /// <summary>
        /// Get the piece over the block.
        /// </summary>
        /// <returns> optional of the piece </returns>
        IPiece? GetPiece(); // equivale a Nullable<IPiece>

        /// <summary>
        /// Set the piece over the block, if piece already exists, it will removed.
        /// </summary>
        /// <param name="piece"> the piece to be placed in the block </param>
        void SetPiece(IPiece piece);

        /// <summary>
        /// Remove the piece over the block.
        /// </summary>
        /// <returns> the piece removed </returns>
        IPiece? RemovePiece();

        /// <summary>
        /// Check if the block contains a piece.
        /// </summary>
        /// <returns> true if the piece exists </returns>
        bool PieceExists();

    }
}
