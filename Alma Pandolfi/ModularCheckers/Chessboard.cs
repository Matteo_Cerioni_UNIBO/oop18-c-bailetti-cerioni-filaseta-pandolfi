﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace ModularCheckers
{
    /// <summary>
    /// Chessboard implementation.
    /// </summary>
    class Chessboard : IChessboard
    {
        /// <summary>
        /// Default width of the Chessboard.
        /// </summary>
        public static int DEFAULT_WIDTH = 8;

        /// <summary>
        /// Default height of the Chessboard.
        /// </summary>
        public static int DEFAULT_HEIGHT = 8;

        private Dictionary<Coordinate, Block> Map = new Dictionary<Coordinate, Block>();
        private int Width;
        private int Height;

        /// <summary>
        /// Initialize the map with the parameterised dimension.
        /// </summary>
        /// <param name="width"> width of the Chessboard </param>
        /// <param name="height"> height of the Chessboard </param>
        public Chessboard(int width, int height)
        {
            if (width <= 0 || height <= 0)
            {
                throw new ArgumentException("Cannot create a Chessboard with width or height <= 0!");
            }

            this.Width = width;
            this.Height = height;

            for (int w = 0; w < width; w++)
            {
                for (int h = 0; h < height; h++)
                {
                    Coordinate key = new Coordinate(w, h);
                    Block value = new Block();
                    this.Map.Add(key, value);
                }
            }
        }

        /// <summary>
        /// Initialize the map with the parameterised dimension.
        /// </summary>
        public Chessboard() : this(DEFAULT_WIDTH, DEFAULT_HEIGHT) { }

        /// <inheritdoc />
        public Block GetBlock(Coordinate coordinate)
        {
            if (this.Map.ContainsKey(coordinate))
            {
                return this.Map[coordinate];
            }
            throw new ArgumentException("Block not found");
        }

        /// <inheritdoc />
        public Pair<int, int> GetSize()
        {
            return new Pair<int, int>(this.Width, this.Height);
        }

        /// <inheritdoc />
        public IDictionary<Coordinate, Block> GetBlocks()
        {
            return this.Map.ToImmutableDictionary();
        }

        /// <inheritdoc />
        public void Reset()
        {
            foreach (IBlock b in Map.Values)
            {
                b.RemovePiece();
            }
        }
    }
}

