﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ModularCheckers
{
    /// <summary>
    /// Test class for Man class.
    /// </summary>
    [TestClass]
    public class PieceTest
    {
        /// <summary>
        /// Testing color.
        /// </summary>
        [TestMethod]
        public void TestBasic()
        {
            IPiece p = new Man(Color.White);
            Assert.AreEqual(Color.White, p.GetColor(), "Color change");
        }

        /// <summary>
        /// Testing Man.
        /// </summary>
        [TestMethod]
        public void TestMan()
        {
            IPiece p = new Man(Color.Black);
            Assert.AreEqual(PieceType.Man, p.GetPieceType(), "PieceType must be a man");
            IEnumerable<Coordinate> moveSet = p.GetMoveSet();
            List<Coordinate> list = moveSet.ToList();
            Assert.AreEqual(2, list.Count, "Man should have 2 moves");
            List<Coordinate> expectedList = new List<Coordinate>();
            expectedList.Add(new Coordinate(1, 1));
            expectedList.Add(new Coordinate(-1, 1));
            foreach(Coordinate c in list)
            {
                Assert.IsTrue(list.Contains(c), "Man can't make these moves");
            }
        }
    }
}
