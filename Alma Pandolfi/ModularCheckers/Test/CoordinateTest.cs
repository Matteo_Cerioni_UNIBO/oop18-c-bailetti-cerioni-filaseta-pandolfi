using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ModularCheckers
{
    /// <summary>
    /// Test class for Coordinate class.
    /// </summary>
    [TestClass]
    public class CoordinateTest
    {
        /// <summary>
        /// Testing coordinate constructor & get methods.
        /// </summary>
        [TestMethod]
        public void TestBasic()
        {
            Coordinate c = new Coordinate(5, 10);
            Assert.AreEqual((int)5, c.GetX(), "X must be equals to 5");
            Assert.AreEqual((int)10, c.GetY(), "Y must be equals to 10");

        }

        /// <summary>
        /// Testing coordinate equals.
        /// </summary>
        [TestMethod]
        public void TestEquals()
        {
            Coordinate c1 = new Coordinate(15, 25);
            Coordinate c2 = new Coordinate(15, 25);
            Coordinate c3 = new Coordinate(15, 22);
            Assert.AreNotEqual(c1, c3);
            Assert.AreEqual(c1, c2, "C1 must be equals to C2");
        }
    }
}