﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ModularCheckers
{
    /// <summary>
    /// Test class for chessboard.
    /// </summary>
    [TestClass]
    public class ChessboardTest
    {
        private static IChessboard InitChessboard(int x, int y)
        {
            return new Chessboard(x, y);
        }

        /// <summary>
        /// Test the size and the initialization of the chessboard and related blocks.
        /// </summary>
        [TestMethod]
        public void TestBasic()
        {
            IChessboard chessboard = InitChessboard(10, 10);
            Assert.AreEqual(chessboard.GetSize(), new Pair<int, int>(10, 10), "Chessboard size must be the size passed in constructor");
            Assert.AreEqual(chessboard.GetBlocks().Count, 10 * 10, "Map size must be x*y");
            Assert.IsNotNull(chessboard.GetBlock(new Coordinate(1, 1)), "Blocks must be initialized");
            Block firstBlock = chessboard.GetBlock(new Coordinate(0, 0));
            Assert.IsNotNull(firstBlock, "Coordinates must start with 0");
            Assert.IsFalse(firstBlock.PieceExists(), "Blocks must be empty");
        }

        /// <summary>
        /// test the unmodifiable map returned by getBlocks methods.
        /// </summary>
        [TestMethod]
        public void TestUnmodifiableMap()
        {
            IChessboard chessboard = InitChessboard(8, 8);
            try
            {
                chessboard.GetBlocks().Add(new Coordinate(1, 2), new Block());
                Assert.Fail("getBlocks must return an Unmodifiable Map");
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Exception thrown correctly: " + e.ToString());
            }
        }

        /// <summary>
        /// Test the reset method.
        /// </summary>
        [TestMethod]
        public void TestReset()
        {
            IChessboard chessboard = InitChessboard(8, 8);
            Coordinate c = new Coordinate(1, 1);
            chessboard.GetBlock(c).SetPiece(new Man(Color.White));
            chessboard.Reset();
            Assert.IsFalse(chessboard.GetBlock(c).PieceExists(), "Reset must remove all pieces from the blocks!");
        }
    }
}