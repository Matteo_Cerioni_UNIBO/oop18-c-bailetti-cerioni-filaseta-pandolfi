﻿namespace ModularCheckers
{
    /// <summary>
    /// Color Enum.
    /// Colors that can be used.
    /// </summary>
    enum Color
    {
        /// <summary>
        /// White color.
        /// </summary>
        White,

        /// <summary>
        /// Black color.
        /// </summary>
        Black
    }
}
