﻿using System;

namespace ModularCheckers
{
    /// <summary>
    /// Block implementation.
    /// </summary>
    [Serializable]
    class Block : IBlock
    {
        private IPiece? piece = null;

        /// <summary>
        /// Get the piece.
        /// </summary>
        /// <returns> the Optional of the piece over the block </returns>
        public IPiece? GetPiece()
        {
            return this.piece;
        }

        /// <summary>
        /// Set the piece over the block , overwriting current piece.
        /// </summary>
        /// <param name="piece"> the piece to set </param>
        public void SetPiece(IPiece piece)
        {
            this.piece = piece;
        }

        /// <summary>
        /// Remove the piece over the block.
        /// </summary>
        /// <returns> the removed piece </returns>
        public IPiece? RemovePiece()
        {
            IPiece? p = this.GetPiece();
            this.piece = null;
            return p;
        }

        /// <summary>
        /// Check if exists the piece over the block.
        /// </summary>
        /// <returns> true if piece exists </returns>
        public bool PieceExists()
        {
            return this.piece != null;
        }
    }
}