﻿using System.Collections.Generic;

namespace ModularCheckers
{
    /// <summary>
    /// AbstractPiece implementing GetColor
    /// </summary>
     abstract class AbstractPiece : IPiece
    {
        private Color color;

        /// <summary>
        /// Base constructor.
        /// </summary>
        /// <param name="color"> is the color of the piece </param>
        public AbstractPiece(Color color)
        {
            this.color = color;
        }

        /// <inheritdoc />
        public Color GetColor()
        {
            return color;
        }

        /// <inheritdoc />
        public abstract IEnumerable<Coordinate> GetMoveSet();

        /// <inheritdoc />
        public abstract PieceType GetPieceType();

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (GetType() != obj.GetType())
            {
                return false;
            }
            AbstractPiece other = (AbstractPiece)obj;

            return color == other.color;
        }
        /// <inheritdoc />
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result +  color.GetHashCode();
            return result;
        }
    }
}