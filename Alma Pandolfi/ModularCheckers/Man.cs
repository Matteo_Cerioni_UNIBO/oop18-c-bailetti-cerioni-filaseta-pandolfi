﻿using System.Collections.Generic;

namespace ModularCheckers
{
    class Man : AbstractPiece
    {
        /// <summary>
        /// Initialize a Man.
        /// </summary>
        /// <param name="color"> the color of the Man </param>
        public Man(Color color) : base(color) { }

        /// <inheritdoc />
        public override PieceType GetPieceType()
        {
            return PieceType.Man;
        }

        /// <inheritdoc />
        public override IEnumerable<Coordinate> GetMoveSet()
        {
            return new List<Coordinate>() { new Coordinate(1, 1), new Coordinate(-1, 1) };
        }
    }
}
