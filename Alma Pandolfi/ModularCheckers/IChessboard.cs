﻿using System.Collections.Generic;

namespace ModularCheckers
{
    /// <summary>
    /// Interface for the Chessboard class.
    /// </summary>
    interface IChessboard
    {
        /// <summary>
        /// Get the Block in the coordinate.
        /// </summary>
        /// <param name="coordinate"> the coordinate where get the block </param>
        /// <returns> the block in the coordinate </returns>
        Block GetBlock(Coordinate coordinate);

        /// <summary>
        /// Get the size of the chessboard.
        /// </summary>
        /// <returns> a pair with the x and y components of the chessboard size </returns>
        Pair<int, int> GetSize();

        /// <summary>
        /// Get all the blocks of the chessboard.
        /// </summary>
        /// <returns> the unmodifiable map of the blocks </returns>
        IDictionary<Coordinate, Block> GetBlocks();

        /// <summary>
        /// Remove all the pieces of the chessboard.
        /// </summary>
        void Reset();
    }
}