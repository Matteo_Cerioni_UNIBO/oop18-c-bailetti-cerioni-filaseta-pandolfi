﻿using System.Collections.Generic;

namespace ModularCheckers
{
    /// <summary>
    /// Interface for the piece class.
    /// </summary>
    interface IPiece
    {
        /// <summary>
        /// Get the type of the piece.
        /// </summary>
        /// <returns> type of the piece </returns>
        PieceType GetPieceType();

        /// <summary>
        /// Get the color of the piece.
        /// </summary>
        /// <returns> color of the piece </returns>
        Color GetColor();

        /// <summary>
        /// Get the relative moveSet of the piece.
        /// </summary>
        /// <returns> the relative moveSet </returns>
        IEnumerable<Coordinate> GetMoveSet();
    }
}