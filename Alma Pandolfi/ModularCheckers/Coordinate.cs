﻿using System;

namespace ModularCheckers
{
    [Serializable]
    /// <summary>
    /// Coordinate class.
    /// </summary>
    public class Coordinate : Pair<int, int>
    {
        /// <summary>
        /// Constant used for invert numbers.
        /// </summary>
        private const int INVERTER = -1;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Coordinate(int x, int y) : base(x, y) { }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="coordinate"> coordinate to copy </param>
        public Coordinate(Coordinate coordinate) : base(coordinate.GetX(), coordinate.GetY()) { }

        /// <summary>
        /// Invert the Y element.
        /// </summary>
        public void InvertY()
        {
            this.SetY(this.GetY() * INVERTER);
        }

        /// <summary>
        /// Translate this coordinate to another.
        /// </summary>
        /// <param name="to"> coordinate to translate </param>
        public void Translate(Coordinate to)
        {
            this.SetY(this.GetY() + to.GetY());
            this.SetX(this.GetX() + to.GetX());
        }

        /// <summary>
        /// Check if the coordinate is inside of the specified borders.
        /// </summary>
        /// <param name="minX"> minimum x value </param>
        /// <param name="minY"> minimum y value </param>
        /// <param name="maxX"> maximum x value </param>
        /// <param name="maxY"> maximum y value </param>
        /// <returns> true if is inside, else false </returns>
        public bool isInsideBorders(int minX, int minY, int maxX, int maxY)
        {
            return this.GetX() >= minX && this.GetX() <= maxX && this.GetY() >= minY && this.GetY() <= maxY;
        }

    }
}


